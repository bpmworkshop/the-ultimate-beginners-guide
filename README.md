The Ultimate Beginners Guide 
============================
There are so many ways and so much content out there to get you started with open source 
rules and process automation tooling by Red Hat, so where do you start? The goal of this 
guide is to provide you with a learning path through the provided content as a way to get 
started from download, to installation, to hands-on workshops, and all the way to maximizing 
your ability to use this technology in a cloud-native container-based manner.

You'll walk away from this guide with a list of content that's enjoyable to work with and 
can help you get started on your rules and process automation projects today.

These slides are  online at: [https://bpmworkshop.gitlab.io/the-ultimate-beginners-guide](https://bpmworkshop.gitlab.io/the-ultimate-beginners-guide)

![Cover Slide](cover.png)
